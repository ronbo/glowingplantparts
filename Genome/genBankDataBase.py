"""
    Heather Lee Wilson August 2013
    License: Creative Commons with attribution v3.0
    http://creativecommons.org/licenses/by/3.0/us/legalcode
"""
import os
import sqlite3
import sys
from Bio import Entrez
import genBankReadNNSB

executable_path = os.path.abspath(os.path.dirname(__file__))
def iterate_through_session_ids(session_list_file_name, file_parser):
	Entrez.email = 'mizheatherlee@gmail.com'
       	database_path = os.path.join(executable_path , "..", "data", "sequences.db")
	con = sqlite3.connect(database_path)
	with con:
		con.row_factory = sqlite3.Row		
		cur = con.cursor()
		cur.execute("Drop TABLE IF EXISTS GenBank")
		cur.execute("CREATE TABLE GenBank(Id INTEGER PRIMARY KEY AUTOINCREMENT, GeneName TEXT, TotalSequence TEXT, SeqPrefix TEXT, SeqFollowing TEXT, Record TEXT)")


		session_ids_file = open(session_list_file_name, 'r')
        	session_number = session_ids_file.readline().split()[0]
		while session_number != "":
			session_number = session_number.split()[0]
			handle = Entrez.efetch(db="nucleotide", id=session_number, rettype="gb", retmode="text")
			list_of_tuples = file_parser.get_data_tuples_for_file_handle(handle, session_number)
			cur.executemany('INSERT INTO GenBank(GeneName, TotalSequence, SeqPrefix, SeqFollowing, Record) VALUES (?, ?, ?, ?, ?)', list_of_tuples)
			handle.close()
			session_number = session_ids_file.readline()
			print "Session Number: " + session_number
	session_ids_file.close()

filepath = os.path.join(executable_path, "..", "data", "listOfGenBankSessionIds.dat");
iterate_through_session_ids(filepath, genBankReadNNSB)

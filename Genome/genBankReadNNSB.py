#!/usr/bin/env python
""" 
    Example of biopython genbank parsing
    
    Ron Shigeta, Heather Lee Wilson August 2013
    License: Creative Commons with attribution v3.0
    http://creativecommons.org/licenses/by/3.0/us/legalcode
    extract promotor  or whole gene sequences for a given gene
    
	
"""

""" imports """
import sys
import string
import Bio
from Bio import Entrez
from Bio import SeqIO
import csv
import sqlite3
import collections

""" globals """
NAME_INDEX = 0
START_INDEX = 1
STOP_INDEX = 2
STRAND_INDEX = 3


GenBankRecord = collections.namedtuple('GenBankRecord', 'gene_name, total_sequence, prefix_sequence, suffix_sequence, session_id')

def fetchGenBankForSessionNumber(sessionNumber):
	# hdub do this earlier
	Entrez.email = "mizheatherlee@gmail.com"
	handle = Entrez.efetch(db="nucleotide", id=sessionNumber, rettype="gb", retmode="text")	
	seq_record = SeqIO.read(handle, "fasta")
	handle.close()

def extract_feature_tokens(feature, type):
    if len(type) > 0 and feature.type !=type:
	return None

    # 'gene', CDS, mobile_element
    strand = "+" if feature.strand == 1 else "-"
    
    otherFeatures = None
    try:
        "\t".join(feature.qualifiers['db_xref'])
    except KeyError as e:
        #No other features
	pass

    if 'gene' in feature.qualifiers.keys():
        gene_name = feature.qualifiers['gene'][0]
    else:
        gene_name = "N/A"
    
    try:
        return (gene_name, feature.location.start.position,
            feature.location.end.position, strand, otherFeatures )
    except Error as e:
	print "Error! " + e.strerror
        return None

def extract_pos_chromosome_sebsequence(name, record, is_pos_strand, subseq_start, subseq_stop):
    if subseq_stop > len(record.seq):
        subseq_stop = len(record.seq) 
    retseq = ''.join([record.seq[x] for x in range(subseq_start, subseq_stop)])
    if not is_pos_strand:
        #dealing with the reverse strand
        retseq = Bio.Seq.reverse_complement(retseq)
    return retseq

def get_data_tuples_for_file_handle(file_handle, session_id):
	entries = [] 
	for record in SeqIO.parse(file_handle,"gb"):
	# gather all the genes for this file, and put in a list of tuples to be databased
		for feature in record.features:
			token = extract_feature_tokens(feature,"CDS")
			if token is None:
				continue #if no token was found, just ignore and move on to the next feature
			name = token[NAME_INDEX]
			strand = token[STRAND_INDEX]
			start = token[START_INDEX]
			stop = token[STOP_INDEX]

			if strand == '+':
			    is_pos_strand = True
			    subseq_start = start - 20
			    subseq_stop = start + 48
			elif strand == '-':
			    is_pos_strand = False
			    subseq_start = stop - 48
			    subseq_stop = stop + 20
			else:
			    continue

			retseq = extract_pos_chromosome_sebsequence(name, record, is_pos_strand, subseq_start, subseq_stop)
			
			entry = GenBankRecord._make([name,  retseq, retseq[0:20], retseq[20:48], session_id])
			entries.append(entry)
	return entries

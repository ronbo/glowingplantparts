#!/usr/bin/env python

""" 
    Example of biopython genbank parsing
    
    Carl Gorringe Jess Yu, Cristina Deptula, Alex Alekseyenko
    Ron Shigeta August 2013
    License: Creative Commons with attribution v3.0
    http://creativecommons.org/licenses/by/3.0/us/legalcode
    extract promotor  or whole gene sequences for a given gene
    
    ARGUMENTS:
    1) FILENAME (mandatory)
    2) Gene (optional) - without a specific locus ID, the script will evaluate for all known cds and gene records
"""

""" imports """
import sys
import string
from Bio import Entrez
from Bio import SeqIO

""" globals """
gbpath = "" 

def printFeature(feature,type):
    if len(type) > 0 and feature.type !=type:
        return
        # 'gene', CDS, mobile_element
    strand = "+" if feature.strand == 1 else "-"
    otherFeatures = "\t".join(feature.qualifiers['db_xref'])
    toks = ( feature.qualifiers['gene'][0], feature.qualifiers['locus_tag'][0], feature.location.start.position,
        feature.location.end.position, strand, otherFeatures )

    seq = record.seq[feature.location.start.position - 1:feature.location.end.position - 1]
    pos = seq.find('ATG')
    if pos > 0:
        print "\t".join(map(str,toks) )
        print pos
        startp = pos - 20
        if startp < 0:
            startp = 0
        endp = pos + 18
        print seq[startp:endp]
        print "----------"

filein = sys.argv[1]
#filein = 'NC_000932.gb'

try: 
    fin = open(filein, 'r') 
except:
    sys.exit("Can\'t open %s" % (filein) )

filePtr= SeqIO.parse(fin,"gb")
"""
there is only one sequence in this file
if there are more than one, can use:
for record in filePtr.next():
        also useful:
    from dir(record)
    record.seq is the chromosome sequence
    record.name / id - the accession
    record.description
"""
record = filePtr.next()
# print all the genes
for feature in record.features:
    printFeature(feature,"gene") 
    # do stuff here -> extractFeature()

fin.close()


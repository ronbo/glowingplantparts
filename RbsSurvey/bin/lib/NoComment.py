import sys, re
import csv

class NoComment:
    """
    NoComment
    Ron Shigeta
    August 2013
    License: Creative Commons Attribution 3.0 License 
    http://creativecommons.org/licenses/by/3.0/us/legalcode
    Wrapper class to strip comments off of input before parsing

    f is an opened file handler 

    usage:
    import NoComment 
    tsv_file = csv.reader( NoComment.NoComment( open(filename,"r"), delimiter = '\t' )
    for row in tsv_file:
        doSomething()

    or: 
    This is the only way I know of to get access to the class internals directly

    nc = NoComment(open(filename,"r"), "#")
    csvr = csv.reader(nc,delimiter='\t')
    for row in csvr:
        doSomething()
    print nc.commentCount
    
    """

    
    def __init__(self,f,commentstring="#"):
        self.f = f
        self.commentstring = commentstring
        self.lastComment = ""
        self.comments = []
        self.commentCount = 0 

    def next(self):
        line = self.f.next()
        while line.startswith( self.commentstring ):
            self.comments.append(line)
            self.commentCount += 1
            self.lastComment = line
            line = self.f.next()
        return line    

    def __iter__(self):
        return self

import sys
from Bio.Seq import reverse_complement


""" 
	Team:
 	Carl Gorringe
 	Cristina Deptula
 	Niranjana Nagarajan
 	Tony Hecht
 	Shriram Bharath

 	Usage: python at_genome_extract.py Chr1 ../../datain/TAIR10/chr1.fa.sns
"""



#Chr1    TAIR10  protein 3760    5630    .       +       .       ID=AT1G01010.1-Protein;Name=AT1G01010.1;Derives_from=AT1G01010.1
def extract_gff_mapping(seqname, filename):
	return_ranges = []
	for line in open(filename, 'r'):
		line = line.strip()
		cols = line.split('\t')
		if len(cols) != 9:
			continue

		sns_file = cols[0].strip()
		feature = cols[2].strip()
		start = int(cols[3])
		stop = int(cols[4])
		strand = cols[6].strip()
		name = cols[8].strip()

		if sns_file.lower() == seqname.lower() and feature == 'protein':
			return_ranges.append([start, stop, strand, cols])

	return return_ranges


def load_chr_file(filename):
	with open(filename, 'r') as f:
		read_data = f.read()
	f.close()
	return read_data


def main():
	if len(sys.argv) != 3:
		print "Usage: at_genome_extract.py <seqname> <snsfile>"
		return

	seqname = sys.argv[1].strip()
	snsfile = sys.argv[2].strip()

	return_ranges = extract_gff_mapping(seqname, '../../datain/TAIR10/TAIR10_GFF3_genes.gff')
	read_data = load_chr_file('../../datain/TAIR10/chr5.fa.sns')

	for [start, stop, strand, cols] in return_ranges:
		if strand == '.':
			raise Exception("Invalid strand")
		line = "\t".join(cols) + "\t" + reverse_complement(read_data[start-1:stop]) if strand == '-' else read_data[start-1:stop]
		print line

main()

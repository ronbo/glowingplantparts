#!/usr/bin/env python

""" 
    Example of biopython genbank parsing
    Alex Alekseyenko, Jessica Yu, Carl Gorringe, Cristina Depula
    
    Ron Shigeta August 2013
    License: Creative Commons with attribution v3.0
    http://creativecommons.org/licenses/by/3.0/us/legalcode
    extract promotor  or whole gene sequences for a given gene
    Written by Ron Shigeta 
    
    ARGUMENTS:
    1) FILENAME (mandatory)
"""

import sys
import string
from Bio import SeqIO
from Bio.Seq import Seq


def printFeature(feature):

    # things we'll need to print for the assignment
    gene = feature.qualifiers.get('gene', ["N/A"])
    start_seq = feature.location.start.position - 1
    end_seq = feature.location.end.position - 1
    strand = feature.strand

    # figured these might be good to have too
    locus_tag = feature.qualifiers.get('locus_tag', ["N/A"])
    db_xref = feature.qualifiers.get('db_xref', ["N/A"])

    # crop the sequence of this feature
    seq = record.seq[start_seq:end_seq]

    # if the strand needs a reverse complement...
    if strand != 1:
        seq = Seq.reverse_complement(seq)

    # find the start codon
    pos = seq.find('ATG')

    # if found, try to return 20 bases on either side
    if pos > 0:
        startp = max((pos-20),0)
        endp = min((pos+23),len(seq))
        print ("Matching sequence: " + seq[startp:endp])
        print ("Index (absolute): " + str(start_seq))
        print ("Index (relative): " + str(pos))
    else:
        print "/"*21
        print "Start codon not found"
        print "\\"*21

    # either way, tell us where we are
    print ("Gene: " + str(gene[0]))
    print ("Strand: " + str(strand))
    print ("Locus Tag: " + str(locus_tag[0]))
    print ("DB Xref: " + str(db_xref[0]))
    print
    print

# read file name
if sys.argv[1]:
    filename = sys.argv[1]
else:
    sys.exit("Provide filename as first argument")

# try loading the file
try: 
    fin = open(filename, 'r') 
except:
    sys.exit("Can\'t open {0}".format(filename))

# parse the .gb file using SeqIO
filePtr= SeqIO.parse(fin,"gb")
    #            there is only one sequence in this file
    #            if there are more than one, can use:
    #            for record in filePtr.next():
    #                    also useful:
    #                from dir(record)
    #                record.seq is the chromosome sequence
    #                record.name / id - the accession
    #                record.description
record = filePtr.next()

# for every gene in the record...
for feature in record.features:
    if feature.type == 'gene':
        printFeature(feature)

# clean up after yourself
fin.close()

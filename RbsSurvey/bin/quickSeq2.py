from Bio import SeqIO

"""
    QuickSeek Example
    Read any number of genome segments super fast from sns files

    Ron Shigeta August 2013
    License: Creative Commons with attribution v3.0
    http://creativecommons.org/licenses/by/3.0/us/legalcode
    extract promotor  or whole gene sequences for a given gene 

    Prepare sns (sequence no space ) file from a fasta file
    tail +1 | tr -d '\n'  > chrM.fa.sns

"""

# normally you open and create an array of file pointers - one for each chromosome 

filename = "chr1.fa.sns"
fin = open(filename, "r")
""" 
    example 1   AT1G01010.1 Protein Record
    get sequence from 10000 to 10050
"""
start = 28088-20 
stop = start+40
strand = "+"
length = stop - start + 1 
if length < 0:
    quit("negative length!")

"""
    NOTE:
        Genome coordinages start at 1
        Filepointers start counting at 0
"""
fin.seek(start-1, 0)
seq = fin.read(length )
print seq
fin.close()



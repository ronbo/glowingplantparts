#!/usr/bin/env python

""" 
    Example of biopython genbank parsing
    
    Ron Shigeta August 2013
    License: Creative Commons with attribution v3.0
    http://creativecommons.org/licenses/by/3.0/us/legalcode
    extract promotor  or whole gene sequences for a given gene
    Written by Ron Shigeta 
    
    ARGUMENTS:
    1) FILENAME (mandatory)
    2) Gene (optional) - without a specific locus ID, the script will evaluate for all known cds and gene records
"""

""" imports """
import sys
import string
import Bio
from Bio import Entrez
from Bio import SeqIO


""" globals """
NAME_INDEX = 1
START_INDEX = 2
STOP_INDEX = 3
STRAND_INDEX = 4

gbpath = "" 
filein = '../../datain/NC_000932.gb'




def extract_feature_tokens(feature, type):
    if len(type) > 0 and feature.type !=type:
        return None

    # 'gene', CDS, mobile_element
    strand = "+" if feature.strand == 1 else "-"
    otherFeatures = "\t".join(feature.qualifiers['db_xref'])
    try:
        return ( feature.qualifiers['gene'][0], feature.qualifiers['locus_tag'][0], feature.location.start.position,
            feature.location.end.position, strand, otherFeatures )
    except:
        return None



def extract_pos_chromosome_sebsequence(name, is_pos_strand, subseq_start, subseq_stop):
    retseq = ''.join([record.seq[x] for x in range(subseq_start, subseq_stop)])
    if not is_pos_strand:
        #dealing with the reverse strand
        retseq = Bio.Seq.reverse_complement(retseq)
    return retseq



try: 
    fin = open(filein, 'r') 
except:
    sys.exit("Can\'t open %s" % (filein) )

filePtr= SeqIO.parse(fin,"gb")
"""
there is only one sequence in this file
if there are more than one, can use:
for record in filePtr.next():
        also useful:
    from dir(record)
    record.seq is the chromosome sequence
    record.name / id - the accession
    record.description
"""
record = filePtr.next()
# print all the genes

for feature in record.features:
    token = extract_feature_tokens(feature,"CDS") 
    if not token:
        continue #if no token was found, just ignore and move on to the next feature

    name = token[NAME_INDEX].strip()
    strand = token[STRAND_INDEX].strip()
    start = token[START_INDEX]
    stop = token[STOP_INDEX]

    if strand == '+':
        is_pos_strand = True
        subseq_start = start - 20
        subseq_stop = start + 23
    elif strand == '-':
        is_pos_strand = False
        subseq_start = stop - 23
        subseq_stop = stop + 20
    else:
        continue

    retseq = extract_pos_chromosome_sebsequence(name, is_pos_strand, subseq_start, subseq_stop)
    #will print in this order - 
    #   1. Name
    #   2. Positive strand sequence - 43 genes
    #   3. Tokens from the record
    #   4. Breakdown - first 20 chars
    #   5. Breakdown - second set 3 chars (ATG)
    #   6. Breakdown - final 20 chars
    print name, '\t', retseq, '\t', token, '\t', retseq[0:20], '\t', retseq[20:23], '\t', retseq[23:]


fin.close()
